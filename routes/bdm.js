var express = require('express');
var router = express.Router();
var bdm = require('../helpers/bonita.bdm');
var logger = require('../log/logger')
var _ = require('lodash');

router.post("/sale", (req, res) => {
    logger.info(`LLAMADA /api/sale body:${JSON.stringify(req.body)}`)

    bdm.GetSale()
        .then((response) => {
            res.send(response).status(200)
        }).catch(e => {
            res.json(e.response.data).status(e.response.status)
        })
})

router.post("/cards", (req, res) => {
    logger.info(`LLAMADA /api/sale body:${JSON.stringify(req.body)}`)

    bdm.GetSale()
        .then((sales) => {
            // total vendido
            var aux = _.flatten(sales.map(a => a.items.map(i => i.price)));
            var sum_total = _.reduce(aux, (a, b) => a + b, 0)

            // items vendidos
            aux = _.flatten(sales.map(a => _.reduce(a.items.map(i => i.quantity), (a, b) => a + b, 0)));
            var items_total = _.reduce(aux, (a, b) => a + b, 0)

            // producto mas vendido
            aux = { ..._.flatten(sales.map(a => a.items)) };

            var repetead_products = _.reduce(aux, (a, b) => {
                if (a.length == 0) return [{ ...b }];

                var match = a.find(i => i.product.id == b.product.id);

                if (match) {
                    var index = _.indexOf(a, match);
                    match.quantity += b.quantity;
                    a.splice(index, 1, match);
                    return [...a]
                } else {
                    return [...a, b]
                }

            }, [])

            var max_producto = _.reduce(repetead_products, (a, b) => {

                if (a.items.length == 0 || a.quantity < b.quantity) {
                    return { items: [b], quantity: b.quantity }
                } else if (a.quantity == b.quantity) {
                    if (a.items.find(i => i.product.id == b.product.id) == null) {
                        return { items: [...a.items, b], quantity: a.quantity }
                    }
                }

                return a;

            }, { items: [], quantity: 0 })

            res.send({
                sales: sales,
                sum_total: sum_total,
                items_total: items_total,
                max_producto: max_producto,
                products: repetead_products
            }).status(200)

        }).catch(e => {
            if (e.response)
                res.json(e.response.data).status(e.response.status)
            else
                res.json(e).status(403)
        })
})

module.exports = router;