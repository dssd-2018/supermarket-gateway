var express = require('express');
var router = express.Router();
var bonitaApi = () => require('./../helpers/bonita.api');
var bonitaRepo = require('../helpers/bonita.repository');
var logger = require('../log/logger')

router.post("/", (req, res) => {
    bonitaApi.post("/bonita/loginservice?username=admin&password=1234&redirect=false")
        .then(response => {
            res.send(response.headers).status(response.status)
        })
        .catch(e => {
            logger.error(`ERROR en /api/ ${!!e.response ? "" : e.response.data}`)
            res.send(e)
        })
})

router.post("/process", (req, res) => {
    bonitaRepo.StartCase(req.body)
        .then((response) => res.send(response.data).status(200))
        .catch(e => {
            logger.error(`ERROR en /api/process status:${!!e.response ? "" : e.response.status} data:${!!e.response ? "" : e.response.data}`)
            res.json(e.response.data).status(e.response.status)
        })
})

router.post("/sale", (req, res) => {
    logger.info(`LLAMADA /api/sale body:${JSON.stringify(req.body)}`)

    bonitaRepo.GetCaseByName("SaleProcess")
        .then((r) => {
            req.body.processDefinitionId = r.data[0].id
            bonitaRepo.StartCase(req.body)
                .then((response) => res.send(response.data).status(200))
                .catch(e => {
                    logger.error(`ERROR en /api/sale status:${!!e.response ? "" : e.response.status} data:${!!e.response ? "" : e.response.data}`)
                    res.json(e.response.data).status(e.response.status)
                })
        }).catch(e => {
            logger.error(`ERROR en /api/sale status:${!!e.response ? "" : e.response.status} data:${!!e.response ? "" : e.response.data}`)
            res.json(e.response.data).status(e.response.status)
        })
})

router.post("/catalog", (req, res) => {
    logger.info(`LLAMADA /api/catalog body:${JSON.stringify(req.body)}`)

    bonitaRepo.GetCaseByName("CatalogProcess")
        .then((r) => {

            req.body.processDefinitionId = r.data[0].id

            bonitaRepo.StartCase(req.body)
                .then((response) => res.send(response.data).status(200))
                .catch(e => {
                    logger.error(`ERROR en /api/catalog status:${!!e.response ? "" : e.response.status} data:${!!e.response ? "" : e.response.data}`)
                    res.json(e.response.data).status(e.response.status)
                })
        }).catch(e => {
            logger.error(`ERROR en /api/catalog status:${!!e.response ? "" : e.response.status} data:${!!e.response ? "" : e.response.data}`)
            res.json(e.response.data).status(e.response.status)
        })
})

router.get("/process/:name", (req, res) => {
    logger.info(`LLAMADA /api/process/${req.params.name} body:${JSON.stringify(req.body)}`)

    bonitaRepo.GetCaseByName(req.params.name)
        .then(response => res.send(response.data).status(200))
        .catch(e => {
            logger.error(`ERROR en /api/process/${req.params.name} status:${!!e.response ? "" : e.response.status} data:${!!e.response ? "" : e.response.data}`)
            res.json(e.response.data).status(e.response.status)
        })
})

module.exports = router;