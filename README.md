### Supermarket API Gateway

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Este proyecto actua como intermediario entre Bonita y las Apps externas.
Permite buscar y crear procesos. No requiere auth.

### 



### Endpoints
#### URL Despliegue
- gateway.supermarket-dssd.ml

#### INDIRECTOS
- GET /api/process/{processName} > Retorna la ID del proceso con nombre processName
- POST /api/process > Inicia un proceso segun el id ingresado en el body.
                      El body ingresado debe ser similar al requerido por Bonita para iniciar un proceso.
                      Devuelve los datos del nuevo proceso iniciado.
#### DIRECTOS
- POST /api/sale => body de bonita, y retorno lo de bonita. 
- POST /api/catalog => body de bonita, y retorno lo de bonita. 

##### nota: ver la carpeta con endpoints de postman para ver que body es necesario.

### Ejemplos

Los ejemplos pueden ser descargados desde supermarket-core, dentro de postman.

