var cookie = require('cookie');
const getHeaders = (response) => {
    var cookies = cookie.parse(response.headers['set-cookie'].join(";"));
    var headers = {
        'X-Bonita-API-Token': cookies['X-Bonita-API-Token'],
        'Content-type': 'application/json',
        'Cookie': `JSESSIONID=${cookies['JSESSIONID']};X-Bonita-API-Token=${cookies['X-Bonita-API-Token']}`
    };
    return headers;
};
exports.getHeaders = getHeaders;