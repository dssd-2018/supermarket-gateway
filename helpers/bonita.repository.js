const { getHeaders } = require("./getHeaders");

var bonitaApi = require('./bonita.api');
var bonitaProxy = require('./bonita.proxy');
var logger = require('../log/logger')

const StartCase = function (item) {
    return bonitaProxy().then(response => {
        var headers = getHeaders(response);
        logger.info(`METHOD: StartCase, Headers${JSON.stringify(headers)}`)
        return bonitaApi.post("/bonita/api/bpm/case", item, {
            headers: headers
        })
    })
}

const GetCaseByName = function (name) {
    return bonitaProxy().then(response => {
        var headers = getHeaders(response)
        logger.info(`METHOD: StartCase, Headers${JSON.stringify(headers)}`)
        return bonitaApi.get("/bonita/API/bpm/process?p=0&c=10&f=name=" + name, {
            headers: headers
        })
    })
}

module.exports = {
    StartCase,
    GetCaseByName
}

