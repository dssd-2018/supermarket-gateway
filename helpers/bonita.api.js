var axios = require('axios');
var logger = require('../log/logger')

const bonita_api = process.env.BONITA_URL

const instance = axios.create({
    baseURL: bonita_api,
    timeout: 1000,
    headers: {
        'Content-type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Origin': '*'
    },
});

instance.interceptors.request.use(function (c) {
    if (c) {
        logger.info(`INTERCEPTOR REQUEST. ${c.url} - ${JSON.stringify(c.headers)} - ${JSON.stringify(c.data)}`)
    } else {
        logger.info(`INTERCEPTOR REQUEST. ${JSON.stringify(c)}`)
    }
    return c;
});

instance.interceptors.response.use(function (c) {
    if (c) {
        logger.info(`INTERCEPTOR RESPONSE. ${c.status} - ${JSON.stringify(c.headers)} - ${JSON.stringify(c.data)}`)
    } else {
        logger.info(`INTERCEPTOR RESPONSE. ${JSON.stringify(c)}`)
    }
    return c;
})

module.exports = instance;

