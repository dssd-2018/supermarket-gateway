const { getHeaders } = require("./getHeaders");

var bonitaApi = require('./bonita.api');
var bonitaProxy = require('./bonita.proxy');
var logger = require('../log/logger')
var _ = require('lodash');

var entities = {
    Sale: "ar.edu.unlp.info.dssd.supermarket.bdm.Sale",
    Product: "ar.edu.unlp.info.dssd.supermarket.bdm.Product",
    SaleItem: "ar.edu.unlp.info.dssd.supermarket.bdm.SaleItem",
    ProductType: "ar.edu.unlp.info.dssd.supermarket.bdm.ProductType",
    Employee: "ar.edu.unlp.info.dssd.supermarket.bdm.Employee",
    Coupon: "ar.edu.unlp.info.dssd.supermarket.bdm.Coupon"
}
var clean = [
    "links",
    "persistenceId",
    "persistenceId_string",
    "persistenceVersion",
    "persistenceVersion_string",
]

var GetEntityRelated = (relations) => (e) => {
    return bonitaApi.get("/bonita" + e.href, { headers: headers })
        .then(async (r) => {
            var el = {};
            var childs;

            if (Array.isArray(r.data)) {
                childs = await Promise.all(r.data.map(obtainRelations))
                relations[e.rel] = childs.map(d => _.omit(d, clean));
            } else {
                childs = await obtainRelations(r.data)
                relations[e.rel] = _.omit(r.data, clean);
            }

        })
}

var obtainRelations = async ent => {
    var relations = {}

    if (!!ent.links) {
        var promises = ent.links.map(GetEntityRelated(relations))
        await Promise.all(promises);
    }

    return { ...relations, ..._.omit(ent, clean) }
}

var headers;

const executeNamedQuery = function (entity) {
    return () =>
        bonitaProxy()
            .then(response => {
                headers = getHeaders(response)

                logger.info(`METHOD: executeNamedQuery, Headers:${JSON.stringify(headers)}`)

                return bonitaApi.get(`/bonita/API/bdm/businessData/${entity}?q=find&p=0&c=1000`, {
                    headers: headers
                }).then(async result => {

                    if (Array.isArray(result.data)) {

                    } else {
                        result.data = [result.data];
                    }

                    var entity_filled = await Promise.all(result.data.map(obtainRelations))
                        .catch(e => {
                            logger.error(`ERROR status:${!!e.response ? "" : e.response.status} data:${!!e.response ? "" : e.response.data}`)
                        });

                    return entity_filled;
                })
            }).catch(e => {
                logger.error(`ERROR status:${!!e.response ? "" : e.response.status} data:${!!e.response ? "" : e.response.data}`)
            })
}

module.exports = {
    GetSale: executeNamedQuery(entities.Sale),
    GetProduct: executeNamedQuery(entities.Product),
    GetSaleItem: executeNamedQuery(entities.SaleItem),
    GetProductType: executeNamedQuery(entities.ProductType),
    GetEmployee: executeNamedQuery(entities.Employee),
    GetCoupon: executeNamedQuery(entities.Coupon),
}