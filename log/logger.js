var winston = require('winston')
 
const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.simple()
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'log/error.log', level: 'error', maxFiles: 5 }),
        new winston.transports.File({ filename: 'log/combined.log', maxFiles: 5 })
    ],
    exceptionHandlers: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'log/exceptions.log', maxFiles: 5 })
    ]
});

logger.exceptions.handle(
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'log/exceptions.log', maxFiles: 5 })
);

module.exports = logger;